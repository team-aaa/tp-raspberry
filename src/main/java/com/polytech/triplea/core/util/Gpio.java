package com.polytech.triplea.core.util;

import com.pi4j.io.gpio.*;

import java.util.HashMap;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class Gpio {
    private static final GpioController factory = GpioFactory.getInstance();
    private static final HashMap<Pin, GpioPinDigitalOutput> outputPins = new HashMap<>();
    private static final HashMap<Pin, GpioPinDigitalInput> inputPins = new HashMap<>();

    public static void shutdown() {
        factory.shutdown();
    }

    public static GpioPinDigitalOutput getGpioPinOutput(Pin pin) {
        if (!outputPins.containsKey(pin))
            outputPins.put(pin, factory.provisionDigitalOutputPin(pin));

        return outputPins.get(pin);
    }

    public static GpioPinDigitalInput getGpioPinInput(Pin pin) {
        if (!inputPins.containsKey(pin))
            inputPins.put(pin, factory.provisionDigitalInputPin(pin));

        return inputPins.get(pin);
    }
}
