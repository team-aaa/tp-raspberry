package com.polytech.triplea.core;

/**
 * @author Alexandre
 *         04/11/2015
 */
public interface OnOffState {

    boolean isOn();

    boolean isOff();
}
