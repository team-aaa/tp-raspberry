package com.polytech.triplea.core.compteur;

/**
 * @author Alexandre
 *         04/11/2015
 */
public abstract class Compteur extends Thread {
    protected boolean exit = false;
    private long valeur = 0;

    public long getValeur() {
        return valeur;
    }

    protected void incrementer() {
        valeur++;
    }

    protected void ajouter(int i) {
        valeur += i;
    }

    public void reset() {
        valeur = 0;
    }

    public void exit() {
        exit = true;
    }
}
