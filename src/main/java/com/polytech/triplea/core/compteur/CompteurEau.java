package com.polytech.triplea.core.compteur;

import com.pi4j.io.gpio.Pin;
import com.polytech.triplea.core.util.Gpio;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class CompteurEau extends Compteur {
    public static final long TicParLitre = 450;
    public final int address;

    public CompteurEau(Pin pinNumber) {
        this.address = Gpio.getGpioPinInput(pinNumber).getPin().getAddress();
    }

    public static long getTicParLitre(double litres) {
        return (long) (((double) TicParLitre) * litres);
    }

    public static double getLitreParTic(long tics) {
        return ((double) tics) / ((double) TicParLitre);
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        int oldValue = com.pi4j.wiringpi.Gpio.digitalRead(address), newValue;
        while (!exit) {
            newValue = com.pi4j.wiringpi.Gpio.digitalRead(address);
            if (oldValue == 0 && newValue == 1)
                incrementer();
            oldValue = newValue;
        }
    }

    public double getLitres() {
        return getLitreParTic(getValeur());
    }
}
