package com.polytech.triplea.core.capteur;

import com.pi4j.io.gpio.Pin;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class CapteurNiveau extends OnOffCapteur {
    public CapteurNiveau(Pin pinNumber) {
        super(pinNumber);
    }
}
