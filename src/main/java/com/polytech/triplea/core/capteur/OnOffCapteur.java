package com.polytech.triplea.core.capteur;

import com.pi4j.io.gpio.Pin;
import com.polytech.triplea.core.OnOffState;
import com.polytech.triplea.core.util.Gpio;

/**
 * @author Alexandre
 *         04/11/2015
 */
public abstract class OnOffCapteur implements OnOffState {
    private final int address;

    protected OnOffCapteur(Pin pinNumber) {
        this.address = Gpio.getGpioPinInput(pinNumber).getPin().getAddress();
    }

    public boolean isOn() {
        return com.pi4j.wiringpi.Gpio.digitalRead(address) == 1;
    }

    public boolean isOff() {
        return com.pi4j.wiringpi.Gpio.digitalRead(address) == 0;
    }
}
