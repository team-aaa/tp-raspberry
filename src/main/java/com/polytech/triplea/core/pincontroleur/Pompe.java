package com.polytech.triplea.core.pincontroleur;

import com.pi4j.io.gpio.Pin;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class Pompe extends OnOffControleur {
    public Pompe(Pin pinNumber) {
        super(pinNumber);
    }
}
