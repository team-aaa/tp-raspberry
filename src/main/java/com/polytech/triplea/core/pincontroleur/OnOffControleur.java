package com.polytech.triplea.core.pincontroleur;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.polytech.triplea.core.OnOffState;
import com.polytech.triplea.core.util.Gpio;

/**
 * @author Alexandre
 *         04/11/2015
 */
public abstract class OnOffControleur implements OnOffState {
    private final GpioPinDigitalOutput pin;

    protected OnOffControleur(Pin pinNumber) {
        this.pin = Gpio.getGpioPinOutput(pinNumber);
        pin.setShutdownOptions(true, PinState.LOW);
    }

    public void allumer() {
        pin.high();
    }

    public void eteindre() {
        pin.low();
    }

    public boolean isOn() {
        return pin.getState().isHigh();
    }

    public boolean isOff() {
        return pin.getState().isLow();
    }
}
