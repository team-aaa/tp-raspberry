package com.polytech.triplea.core.pincontroleur;

import com.pi4j.io.gpio.Pin;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class Robinet extends OnOffControleur {
    public Robinet(Pin pinNumber) {
        super(pinNumber);
    }
}
