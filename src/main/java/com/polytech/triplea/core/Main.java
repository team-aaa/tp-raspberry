package com.polytech.triplea.core;

import com.pi4j.io.gpio.RaspiPin;
import com.polytech.triplea.core.capteur.CapteurNiveau;
import com.polytech.triplea.core.compteur.CompteurEau;
import com.polytech.triplea.core.pincontroleur.Pompe;
import com.polytech.triplea.core.pincontroleur.Robinet;
import com.polytech.triplea.core.util.Gpio;

public class Main {

    public static void main(String[] args) throws Exception {
        double nombreDeLitres = Double.parseDouble(args[0]);
        long tics = CompteurEau.getTicParLitre(nombreDeLitres);
        System.out.println("<br>Nombre de litre à pomper : " + nombreDeLitres + "<br>");
        System.out.println("Nombre de tics nécessaires : " + tics + "<br>");
        System.out.println("============ Initialisation ============<br>");
        Robinet robinet = new Robinet(RaspiPin.GPIO_04);
        Pompe pompe = new Pompe(RaspiPin.GPIO_20);
        CapteurNiveau niveau = new CapteurNiveau(RaspiPin.GPIO_00);
        CompteurEau compteur = new CompteurEau(RaspiPin.GPIO_03);
        System.out.println("Compteur d'eau démarré<br>");
        compteur.start();
        System.out.println("Robinet ouvert<br>");
        robinet.allumer();
        System.out.println("Pompe démarrée<br>");
        pompe.allumer();
        boolean end = false;
        long start = System.currentTimeMillis();
        while (!end) {
            long valeur = compteur.getValeur();
            double avance = 100 * ((double) valeur) / tics;
            if (niveau.isOff()) {
                System.out.println("Il ne reste pas assez d'eau, quantité d'eau effectivement pompée : " + compteur.getLitres() + " litres<br>");
                end = true;
            } else if (valeur >= tics) {
                System.out.println("Quantité d'eau pompée : " + compteur.getLitres() + " litres en " + (System.currentTimeMillis() - start) + " ms<br>");
                end = true;
            } else if (avance == ((long) avance)) {
                System.out.println("Avancement : " + ((long) avance) + "%<br>");
            }
            Thread.sleep(25);
        }
        compteur.exit();
        System.out.println("Pompe arrêtée<br>");
        robinet.eteindre();
        System.out.println("Robinet fermé<br>");
        pompe.eteindre();
        Gpio.shutdown();
    }
}
