package com.polytech.triplea.test.capteur;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import com.polytech.triplea.core.capteur.CapteurNiveau;
import com.polytech.triplea.core.util.Gpio;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class CapteurNiveauTest {

    private CapteurNiveau capteurNiveau;
    private GpioPinDigitalOutput pinOutput;

    @Before
    public void setUp() throws Exception {
        pinOutput = Gpio.getGpioPinOutput(RaspiPin.GPIO_01);
        capteurNiveau = new CapteurNiveau(RaspiPin.GPIO_00);
    }

    @Test
    public void testAllumer() throws Exception {
        pinOutput.high();
        TestCase.assertTrue(capteurNiveau.isOn());
    }

    @Test
    public void testEteindre() throws Exception {
        pinOutput.low();
        TestCase.assertTrue(capteurNiveau.isOff());
    }
}