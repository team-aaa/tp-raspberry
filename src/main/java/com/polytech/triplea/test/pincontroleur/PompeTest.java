package com.polytech.triplea.test.pincontroleur;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import com.polytech.triplea.core.pincontroleur.Pompe;
import com.polytech.triplea.core.util.Gpio;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class PompeTest {

    private Pompe pompe;
    private GpioPinDigitalOutput pinOutput;

    @Before
    public void setUp() throws Exception {
        Pin pin = RaspiPin.GPIO_20;
        pinOutput = Gpio.getGpioPinOutput(pin);
        pompe = new Pompe(pin);
    }

    @Test
    public void testIsOn() throws Exception {
        pinOutput.high();
        TestCase.assertTrue(pompe.isOn());
    }

    @Test
    public void testIsOff() throws Exception {
        pinOutput.low();
        TestCase.assertTrue(pompe.isOff());
    }

    @Test
    public void testAllumer() throws Exception {
        testIsOff();
        pompe.allumer();
        TestCase.assertTrue(pompe.isOn());
    }

    @Test
    public void testEteindre() throws Exception {
        testIsOn();
        pompe.eteindre();
        TestCase.assertTrue(pompe.isOff());
    }
}