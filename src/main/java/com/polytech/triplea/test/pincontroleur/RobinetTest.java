package com.polytech.triplea.test.pincontroleur;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import com.polytech.triplea.core.pincontroleur.Robinet;
import com.polytech.triplea.core.util.Gpio;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class RobinetTest {

    private Robinet robinet;
    private GpioPinDigitalOutput pinOutput;

    @Before
    public void setUp() throws Exception {
        Pin pin = RaspiPin.GPIO_04;
        pinOutput = Gpio.getGpioPinOutput(pin);
        robinet = new Robinet(pin);
    }

    @Test
    public void testIsOn() throws Exception {
        pinOutput.high();
        TestCase.assertTrue(robinet.isOn());
    }

    @Test
    public void testIsOff() throws Exception {
        pinOutput.low();
        TestCase.assertTrue(robinet.isOff());
    }

    @Test
    public void testAllumer() throws Exception {
        testIsOff();
        robinet.allumer();
        TestCase.assertTrue(robinet.isOn());
    }

    @Test
    public void testEteindre() throws Exception {
        testIsOn();
        robinet.eteindre();
        TestCase.assertTrue(robinet.isOff());
    }
}
