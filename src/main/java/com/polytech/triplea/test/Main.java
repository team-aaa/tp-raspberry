package com.polytech.triplea.test;

import com.polytech.triplea.test.capteur.CapteurNiveauTest;
import com.polytech.triplea.test.compteur.CompteurEauTest;
import com.polytech.triplea.test.pincontroleur.PompeTest;
import com.polytech.triplea.test.pincontroleur.RobinetTest;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import java.util.ArrayList;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Class> classes = new ArrayList<>();
        classes.add(PompeTest.class);
        classes.add(RobinetTest.class);
        classes.add(CapteurNiveauTest.class);
        classes.add(CompteurEauTest.class);

        JUnitCore junit = new JUnitCore();
        for (Class aClass : classes) {
            Result result = junit.run(aClass);
            if (result.wasSuccessful()) {
                System.out.println(String.format("Succès du test : %s", aClass.getSimpleName()));
            } else {
                System.out.println(String.format("Echec du test : %s", aClass.getSimpleName()));
                result.getFailures().forEach(System.out::println);
            }
        }
    }
}
