package com.polytech.triplea.test.compteur;

import com.pi4j.io.gpio.RaspiPin;
import com.polytech.triplea.core.compteur.CompteurEau;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author Alexandre
 *         04/11/2015
 */
public class CompteurEauTest {

    private CompteurEau compteurEau;

    @Before
    public void setUp() throws Exception {
        compteurEau = new CompteurEau(RaspiPin.GPIO_03);
    }

    @Test
    public void testIncrement() throws InterruptedException, IOException {
        long freq = 225, duration = 1000;
        Runtime.getRuntime().exec("sudo ./blinker_test");
        Thread.sleep(500);
        compteurEau.start();
        Thread.sleep(duration);
        compteurEau.exit();
        TestCase.assertTrue(freq * duration / 1000 <= compteurEau.getValeur());
    }
}